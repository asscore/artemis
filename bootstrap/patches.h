/*
 * Patches
 */

#ifndef _PATCHES_H_
#define _PATCHES_H_

void apply_patches(const char *path);

#endif /* _PATCHES_H_ */
