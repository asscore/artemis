/*
 * Utility functions
 */

#ifndef _UTILS_H_
#define _UTILS_H_

int _strcmp(const char *s1, const char *s2);
int _strtoi(const char *p);

#endif /* _UTILS_H_ */
