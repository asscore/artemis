/*
 * Bootstrap
 */

#include <tamtypes.h>
#include <kernel.h>
#include <sifrpc.h>
#include <loadfile.h>
#include <iopcontrol.h>
#include <iopheap.h>
#include <debug.h>
#include <stdio.h>
#include <string.h>
#include "utils.h"
#include "patches.h"

#ifdef _ENABLE_PATCHES
static int enable_patches = 0;
#endif

#define ELF_MAGIC   0x464c457f
#define ELF_PT_LOAD 1

/* ELF file header */
typedef struct {
	u8 ident[16];	/* Magic number and other info */
	u16 type;		/* Object file type */
	u16 machine;	/* Architecture */
	u32 version;	/* Object file version */
	u32 entry;		/* Entry point virtual address */
	u32 phoff;		/* Program header table file offset */
	u32 shoff;		/* Section header table file offset */
	u32 flags;		/* Processor-specific flags */
	u16 ehsize;		/* ELF header size in bytes */
	u16 phentsize;	/* Program header table entry size */
	u16 phnum;		/* Program header table entry count */
	u16 shentsize;	/* Section header table entry size */
	u16 shnum;		/* Section header table entry count */
	u16 shstrndx;	/* Section header string table index */
} elf_header_t;

/* Program segment header */
typedef struct {
	u32 type;		/* Segment type */
	u32 offset;		/* Segment file offset */
	void *vaddr;	/* Segment virtual address */
	u32 paddr;		/* Segment physical address */
	u32 filesz;		/* Segment size in file */
	u32 memsz;		/* Segment size in memory */
	u32 flags;		/* Segment flags */
	u32 align;		/* Segment alignment */
} elf_pheader_t;

/*
 * Execute an ELF with SifLoadElf + ExecPS2.
 */
void execute_elf(char *elfpath)
{
	int i, ret, fd = 0;
	t_ExecData sifelf;
	elf_header_t boot_header;
	elf_pheader_t boot_pheader;
	char *args[1];

	args[0] = elfpath;

	DEBUG_BGCOLOR(0x400040); /* dark purple */

	ResetEE(0x7f);

	/* clear user memory */
	for (i = 0x00100000; i < 0x02000000; i += 64) {
		__asm__ (
		"\tsq $0, 0(%0) \n"
		"\tsq $0, 16(%0) \n"
		"\tsq $0, 32(%0) \n"
		"\tsq $0, 48(%0) \n"
		:: "r" (i)
		);
	}

	/* clear scratchpad memory */
	memset((void*)0x70000000, 0, 16 * 1024);

	/* hack do not reset IOP when launching ELF from mass */
	if (!(elfpath[0] == 'm' && elfpath[1] == 'a' &&
			elfpath[2] == 's' && elfpath[3] == 's')) {
		/* reset IOP */
		SifInitRpc(0);

		FlushCache(0);
		FlushCache(2);

		/* reload modules */
		SifLoadFileInit();
		SifLoadModule("rom0:SIO2MAN", 0, NULL);
		SifLoadModule("rom0:MCMAN", 0, NULL);
		SifLoadModule("rom0:MCSERV", 0, NULL);
	}

	DEBUG_BGCOLOR(0x004000); /* dark green */

	/* try to load the ELF with SifLoadElf() first */
	memset(&sifelf, 0, sizeof(t_ExecData));
	ret = SifLoadElf(elfpath, &sifelf);
	if (!ret && sifelf.epc) {
		/* exit services */
		fioExit();
		SifLoadFileExit();
		SifExitIopHeap();
		SifExitRpc();

		FlushCache(0);
		FlushCache(2);

		DEBUG_BGCOLOR(0x000000); /* black */

#ifdef _ENABLE_PATCHES
		if (enable_patches)
			apply_patches(elfpath);
#endif

		/* finally, run game ELF... */
		ExecPS2((void*)sifelf.epc, (void*)sifelf.gp, 1, &elfpath);

		SifInitRpc(0);
	}

	DEBUG_BGCOLOR(0x000040); /* dark maroon */

	/* SifLoadElf failed, so try to load the ELF manually */
	fioInit();
	fd = open(elfpath, O_RDONLY); /* open the ELF */
	if (fd < 0) {
		goto error; /* can't open file, exiting... */
	}

	if (read(fd, &boot_header, sizeof(elf_header_t)) != sizeof(elf_header_t)) {
		close(fd);
		goto error; /* can't read header, exiting... */
	}

	/* check ELF magic */
	if ((*(u32*)boot_header.ident) != ELF_MAGIC) {
		close(fd);
		goto error; /* not an ELF file, exiting... */
	}

	/* copy loadable program segments to RAM */
	for (i = 0; i < boot_header.phnum; i++) {
		lseek(fd, boot_header.phoff+(i*sizeof(elf_pheader_t)), SEEK_SET);
		read(fd, &boot_pheader, sizeof(elf_pheader_t));

		if (boot_pheader.type != ELF_PT_LOAD)
			continue;

		lseek(fd, boot_pheader.offset, SEEK_SET);
		read(fd, boot_pheader.vaddr, boot_pheader.filesz);

		if (boot_pheader.memsz > boot_pheader.filesz)
			memset(boot_pheader.vaddr + boot_pheader.filesz, 0,
					boot_pheader.memsz - boot_pheader.filesz);
	}

	close(fd);

	SifInitRpc(0);

	while (!SifIopReset("rom0:UDNL rom0:EELOADCNF", 0));
	while (!SifIopSync());

	/* exit services */
	fioExit();
	SifExitIopHeap();
	SifLoadFileExit();
	SifExitRpc();
	SifExitCmd();

	FlushCache(0); /* write back contents of data cache and invalidate */
	FlushCache(2); /* invalidate contents of instruction cache */

	DEBUG_BGCOLOR(0x000000); /* black */

#ifdef _ENABLE_PATCHES
	if (enable_patches)
		apply_patches(elfpath);
#endif

	/* finally, run game ELF... */
	ExecPS2((u32*)boot_header.entry, 0, 1, args);
error:
	DEBUG_BGCOLOR(0x404040); /* dark gray screen: error */
	SleepThread();
}

/*
 * Main function.
 */
int main(int argc, char *argv[])
{
#ifdef _ENABLE_PATCHES
	enable_patches = _strtoi(argv[1]);
#endif
	execute_elf((char *) argv[0]);

	return 0;
}
