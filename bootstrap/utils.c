/*
 * Utility functions
 */

#include "utils.h"

/*
 * Do not link to strcmp() from libc.
 */
int _strcmp(const char *s1, const char *s2)
{
	register int i = 0;

	while ((s1[i] != 0) && (s1[i] == s2[i]))
		i++;

	if (s1[i] > s2[i])
		return 1;
	else if (s1[i] < s2[i])
		return -1;

	return 0;
}

/*
 * This function converts string to signed integer.
 */
int _strtoi(const char *p)
{
	int k = 1;
	if (!p)
		return 0;

	int r = 0;

	while (*p) {
		if (*p == '-') {
			k = -1;
			p++;
		} else if ((*p < '0') || (*p > '9'))
			return r;
		r = r * 10 + (*p++ - '0');
	}

	r = r * k;

	return r;
}
