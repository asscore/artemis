/*
 * Patches
 */

#include <kernel.h>
#include <osd_config.h>
#include "utils.h"
#include "patches.h"

/*
 * This function retrieve a pattern in a buffer, using a mask.
 */
static u32 *find_pattern_with_mask(u32 *buf, unsigned int bufsize, const u32 *pattern, const u32 *mask, unsigned int len)
{
	unsigned int i, j;

	len /= sizeof(u32);
	bufsize /= sizeof(u32);

	for (i = 0; i < bufsize - len; i++) {
		for (j = 0; j < len; j++) {
			if ((buf[i + j] & mask[j]) != pattern[j])
				break;
		}
		if (j == len)
			return &buf[i];
	}

	return NULL;
}

/*
 * Skip Videos (sceMpegIsEnd) Code - nachbrenner's basic method, based on CMX/bongsan's original idea.
 * Source: http://replay.waybackmachine.org/20040419134616/http://nachbrenner.pcsx2.net/chapter1.html
 * This patch is expected to work with all games using sceMpegIsEnd, for example:
 * SCUS_973.99(God of War I), SLUS_212.42 (Burnout Revenge) and SLES-50613 (Woody Woodpecker: Escape from Buzz Buzzard Park).
 */
int skip_videos_sceMpegIsEnd(void)
{
	static const unsigned int sceMpegIsEndPattern[] = {
		0x8c830040, /* lw	$v1, $0040($a0) */
		0x03e00008, /* jr	$ra */
		0x8c620000, /* lw	$v0, 0($v1) */
	};
	static const unsigned int sceMpegIsEndPattern_mask[] = {
		0xffffffff,
		0xffffffff,
		0xffffffff,
	};

	u32 *ptr;
	ptr = find_pattern_with_mask((u32 *)0x00100000, 0x01ec0000, sceMpegIsEndPattern, sceMpegIsEndPattern_mask, sizeof(sceMpegIsEndPattern));
	if (ptr) {
		_sw(0x24020001, (u32)ptr + 8); /* addiu	$v0, $zero, 1 <- HERE!!! */
		return 1;
	} else
		return 0;
}

/*
 * Applying needed patches.
 */
void apply_patches(const char *path)
{
	if (_strcmp(path, "cdrom0:\\SCUS_973.99;1") == 0) {
		/* Infinite Health */
		_sw(0x46016328, 0x001e0a8c); /* max.s	f12,f12,f01 */
		/* Infinite Magic */
		_sw(0x46016328, 0x001dbf48); /* max.s	f00,f00,f01 */
		/* Infinite Double Jumps */
		_sw(0x00000000, 0x001e8e2c); /* nop */
		/* Soften Option Always Off */
		_sw(0x00000000, 0x0029cc98); /* nop */
	} else if (_strcmp(path, "cdrom0:\\SCUS_974.81;1") == 0) {
		/* Infinite Health */
		_sw(0x46016328, 0x0021bea4); /* max.s	f12,f12,f01 */
		/* Infinite Magic */
		_sw(0x46020028, 0x00216510); /* max.s	f00,f00,f02 */
		/* Infinite Double Jumps */
		_sw(0x00000000, 0x0022662c); /* nop */
	}

	skip_videos_sceMpegIsEnd();
}
