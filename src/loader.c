/*
 * Boot loader (main project file)
 */

#include <kernel.h>
#include <sifrpc.h>
#include <sbv_patches.h>
#include <string.h>
#ifdef _DVD
#include <libcdvd.h>
#endif
#include <libmc.h>
#ifdef _KEYBOARD
#include <libkbd.h>
#endif
#include "dbgprintf.h"
#include "cheatman.h"
#include "configman.h"
#include "elfldr.h"
#include "erlman.h"
#include "gui.h"
#include "irxman.h"
#include "mypad.h"
#include "myutil.h"
#include "version.h"

#define PAD_PORT 0
#define PAD_SLOT 0

#define CURSOR_TOP 80
#define CURSOR_INCREMENT 18

#define MAXIMUM_ITEMS_PER_PAGE 18
#define MAXIMUM_GAMES 512

extern int SCREEN_WIDTH;

static char _bootpath[FIO_PATH_MAX];
static enum dev_id _bootdev = DEV_UNKN;
static u8 _padbuf[256] __attribute__((aligned(64)));

/*
 * TODO use dirname() from libgen.h
 */

static inline size_t chr_idx(const char *s, char c)
{
	size_t i = 0;

	while (s[i] && (s[i] != c))
		i++;

	return (s[i] == c) ? i : -1;
}

static inline size_t last_chr_idx(const char *s, char c)
{
	size_t i = strlen(s);

	while (i && s[--i] != c)
		;

	return (s[i] == c) ? i : -1;
}

static int __dirname(char *filename)
{
	int i;

	if (filename == NULL)
		return -1;

	i = last_chr_idx(filename, '/');
	if (i < 0) {
		i = last_chr_idx(filename, '\\');
		if (i < 0) {
			i = chr_idx(filename, ':');
			if (i < 0)
				return -2;
		}
	}

	filename[i+1] = '\0';
	return 0;
}

#ifndef _DTL_T10000
/*
 * Build pathname based on boot device and filename.
 */
static char *__pathname(const char *name)
{
	static char filename[FIO_PATH_MAX];
	enum dev_id dev;

	filename[0] = '\0';
	dev = get_dev(name);

	/* add boot path if name is relative */
	if (dev == DEV_UNKN)
		strcpy(filename, _bootpath);

	strcat(filename, name);

	if (dev == DEV_CD) {
		strupr(filename);
		strcat(filename, ";1");
	}

	return filename;
}
#endif

int main(int argc, char *argv[])
{
	cheats_t cheats;
	config_t config;
	engine_t engine;
	char configfile[256];
	char cheatfile[256];
	char boot2[256];
	game_t *game = NULL;
	struct padButtonStatus btn;
	u32 old_pad = 0;

	if (argc == 0) {
		strcpy(_bootpath, "host:");
	} else {
		strcpy(_bootpath, argv[0]);
		__dirname(_bootpath);
	}
	_bootdev = get_dev(_bootpath);

	SifInitRpc(0);

	/* setup GS */
	setup_gs();
	begin_frame();
	draw_background(128);
	end_frame();

	D_PRINTF("Build date: %s %s\n", __DATE__, __TIME__);
	D_PRINTF("Booting from: %s\n", _bootpath);
	D_PRINTF("Initializing...\n");

	D_PRINTF("Reading config...\n");
	config_build(&config);
#ifdef _DTL_T10000
	sprintf(configfile, "host:%s", CONFIG_FILE);
#else
	strcpy(configfile, __pathname(CONFIG_FILE));
#endif
	if (config_read_file(&config, configfile) != CONFIG_TRUE)
		fprintf(stderr, "%s: failed to load config - %s\n", __FUNCTION__, config_error_text(&config));
	config_print(&config);

	if (_bootdev != DEV_HOST && config_get_bool(&config, SET_IOP_RESET))
#ifdef _DTL_T10000
		reset_iop("rom0:UDNL");
#else
		reset_iop("rom0:UDNL rom0:EELOADCNF");
#endif

	if (config_get_bool(&config, SET_SBV_PATCHES)) {
		D_PRINTF("Applying SBV patches...\n");
		sbv_patch_enable_lmb();
		sbv_patch_disable_prefix_check();
	}

	if (load_modules(&config) < 0) {
		fprintf(stderr, "%s: failed to load IRX modules\n", __FUNCTION__);
		goto end;
	}

#ifdef _DVD
	sceCdInit(SCECdINoD);
	sceCdDiskReady(0);
#endif

#ifdef _DTL_T10000
	mcInit(MC_TYPE_XMC);
#else
	mcInit(MC_TYPE_MC);
#endif

	padInit(0);
	padPortOpen(PAD_PORT, PAD_SLOT, _padbuf);
	padWaitReady(PAD_PORT, PAD_SLOT);
	padSetMainMode(PAD_PORT, PAD_SLOT, PAD_MMODE_DIGITAL, PAD_MMODE_LOCK);

#ifdef _KEYBOARD
	PS2KbdInit();
#endif

	if (install_erls(&config, &engine) < 0) {
		fprintf(stderr, "%s: failed to install ERLs\n", __FUNCTION__);
		goto end;
	}

	cheats_init(&cheats);
#ifdef _DTL_T10000
	sprintf(cheatfile, "host:%s", config_get_string(&config, SET_CHEATS_FILE));
#else
	strcpy(cheatfile, __pathname(config_get_string(&config, SET_CHEATS_FILE)));
#endif
	if (load_cheats(cheatfile, &cheats) < 0)
		fprintf(stderr, "%s: failed to load cheats from %s\n", __FUNCTION__, cheatfile);

	strcpy(boot2, config_get_string(&config, SET_BOOT2));

	D_PRINTF("Ready.\n");

	char *titles[MAXIMUM_GAMES];
	int ntitles = 0;

	/* initialize arrays to prevent memory corruption */
	int h = 0;
	for (h = 0; h < MAXIMUM_GAMES; h++) {
		titles[h] = NULL;
	}

	/* fill list of games */
	GAMES_FOREACH(game, &cheats.games) {
		if (game != NULL) {
			titles[ntitles] = game->title;
			ntitles++;
		}
	}
	free(game);

	int page = 1;
	int oldpage;
	int starting = 0;
	int cursor = CURSOR_TOP;
	int n, y;
	int selected = 0;
	int helptick = 0;

	int header_alpha = 0;
	int header_move_done = 0;
	int header_x = 0 - SCREEN_WIDTH;
	int content_alpha = 0;
	int content_fadein_done = 0;

	/* main loop */
	while (1) {
		u32 paddata, new_pad;

		y = CURSOR_TOP;
		oldpage = page;

		begin_frame();
		draw_background(128);

		if (header_x < 0) {
			header_x += 12;
			if (header_x >= 0) {
				header_x = 0;
				header_move_done = 1;
			}
		}

		if (header_alpha < 128) {
			header_alpha += 2;
			if (header_alpha >= 128) {
				header_alpha = 128;
			}
		}

		draw_string(30, 39, header_alpha, 29, 0, "Welcome to ARTEMiS");
		draw_string_right(SCREEN_WIDTH - 30, 48, header_alpha, 16, 0, GIT_VERSION);
		draw_header_line(header_x, 68, header_alpha);

		if (header_move_done) {
			if (content_fadein_done) {
				padWaitReady(PAD_PORT, PAD_SLOT);
				if (!padRead(PAD_PORT, PAD_SLOT, &btn))
					continue;

				paddata = 0xFFFF ^ btn.btns;
				new_pad = paddata & ~old_pad;
				old_pad = paddata;

#ifdef _KEYBOARD
				if (!new_pad) {
					char key_press;
					int command;

					if (PS2KbdRead(&key_press)) {
						if (key_press != PS2KBD_ESCAPE_KEY)
							command = key_press;
						else {
							PS2KbdRead(&key_press);
							command = 0x100 + key_press;
						}
						switch (command) {
							case 0x12c: /* 'Up' == Up */
								new_pad = PAD_UP;
								break;
							case 0x12b: /* 'Down' == Down */
								new_pad = PAD_DOWN;
								break;
							case 0x12a: /* 'Left' == L1 */
								new_pad = PAD_L1;
								break;
							case 0x129: /* 'Right' == R1 */
								new_pad = PAD_R1;
								break;
							case 0x00a: /* 'Enter' == Start */
								new_pad = PAD_START;
								break;
							default:
								break;
						}
					}
				}
#endif

				if ((new_pad & PAD_START) || (new_pad & PAD_CROSS)) {
					if (!config_get_bool(&config, SET_ENGINE_INSTALL)) {
						D_PRINTF("Skipping cheats - engine not installed\n");
#ifdef _DVD
						parse_systemcnf(boot2);
#endif
					} else {
						reset_cheats(&engine);
						game = find_game_by_title(titles[selected], &cheats.games);
						if (game != NULL) {
							D_PRINTF("Activate cheats for \"%s\"\n", game->title);
							activate_cheats(game, &engine);
#ifdef _DVD
							sprintf(boot2, "cdrom0:\\%s;1", strstr(game->title, "/ID ") + strlen("/ID "));
#endif
						}
					}
					execute(&config, boot2);
				} else if (new_pad & PAD_DOWN) {
					if (selected < ntitles - 1) {
						/* if we've reached the bottom of a page... */
						if (cursor >= (CURSOR_TOP + ((MAXIMUM_ITEMS_PER_PAGE - 1) * CURSOR_INCREMENT))) {
							page++;
							selected++;
							cursor = CURSOR_TOP;
						} else {
							selected++;
							cursor += CURSOR_INCREMENT;
						}
					}
				} else if (new_pad & PAD_UP) {
					/* move arrow up if we're not at the beginning of the list */
					if (selected != 0) {
						selected--;
						cursor -= CURSOR_INCREMENT;
					}
					/* if we're at the top of a page differant than page 1.. */
					if ((cursor < CURSOR_TOP) & (page > 1)) {
						page--;
						cursor = CURSOR_TOP + ((MAXIMUM_ITEMS_PER_PAGE - 1) * CURSOR_INCREMENT);
					}
				} else if (new_pad & PAD_R1) { /* jump to next page */
					if ((MAXIMUM_ITEMS_PER_PAGE * page) < ntitles) {
						selected = MAXIMUM_ITEMS_PER_PAGE * page;
						cursor = CURSOR_TOP;
						page++;
					}
				} else if (new_pad & PAD_L1) { /* jump to previous page */
					if ((MAXIMUM_ITEMS_PER_PAGE * (page - 1)) > (ntitles - (MAXIMUM_ITEMS_PER_PAGE * (page - 1)))) {
						selected = MAXIMUM_ITEMS_PER_PAGE * (page - 2);
						cursor = CURSOR_TOP;
						page--;
					}
				}
			}

			if (content_alpha < 128) {
				content_alpha += 5;
				if (content_alpha >= 128) {
					content_alpha = 128;
					content_fadein_done = 1;
				}
			}

			/* set the first item (game title) to appear at the top of the page */
			if (page > oldpage)
				starting += MAXIMUM_ITEMS_PER_PAGE;
			else if ((page < oldpage) & (page > 0))
				starting -= MAXIMUM_ITEMS_PER_PAGE;

			/* render game list */
			for (n = starting; n < (MAXIMUM_ITEMS_PER_PAGE * page); n++) {
				if (titles[n] != NULL) {
					draw_string(30, y, content_alpha, 22, 0, titles[n]);
					y += CURSOR_INCREMENT; /* space between each title */
				}
			}

			/* render arrow graphic */
			draw_string(10, cursor, content_alpha, 22, 0, ">");

			/* render help text */
			if (helptick < 128)
				draw_string(30, 420, content_alpha, 22, 0, "Press START or X to boot game.");
			else if (helptick < 256)
				draw_string(30, 420, content_alpha, 22, 0, "Press R1 or L1 to jump between pages.");

			if (helptick >= 256)
				helptick = 0;

			helptick++;
		}

		end_frame();
	}
end:
	D_PRINTF("Exit...\n");

	config_destroy(&config);
	cheats_destroy(&cheats);
	uninstall_erls();
	SleepThread();

	return 1;
}
