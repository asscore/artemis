/*
 * Configuration manager
 */

#ifndef _CONFIGMAN_H_
#define _CONFIGMAN_H_

#include <libconfig.h>

#ifndef CONFIG_FILE
#define CONFIG_FILE		"artemis.conf"
#endif

/* Define some default settings */
#ifndef IOP_RESET
#define IOP_RESET		1
#endif
#ifndef SBV_PATCHES
#define SBV_PATCHES		1
#endif
#ifndef USB_SUPPORT
#define USB_SUPPORT		1
#endif
#ifndef BOOT2_FILE
#define BOOT2_FILE		"mass:/BOOT/BOOT.ELF"
#endif
#ifndef CHEATS_FILE
#define CHEATS_FILE		"cheats.txt"
#endif

#ifndef ENGINE_INSTALL
#define ENGINE_INSTALL	1
#endif
#ifndef ENGINE_ADDR
#define ENGINE_ADDR		0x00080000
#endif

#ifndef SDKLIBS_INSTALL
#define SDKLIBS_INSTALL	0
#endif
#ifndef SDKLIBS_ADDR
#define SDKLIBS_ADDR	0x000c0000
#endif

/* Keys to access different settings in configuration */
#define SET_IOP_RESET		"loader.iop_reset"
#define SET_SBV_PATCHES		"loader.sbv_patches"
#define SET_USB_SUPPORT		"loader.usb_support"
#define SET_BOOT2			"loader.boot2"
#define SET_CHEATS_FILE		"loader.cheats"
#define SET_ENGINE_INSTALL	"engine.install"
#define SET_ENGINE_ADDR		"engine.addr"
#define SET_SDKLIBS_INSTALL	"sdklibs.install"
#define SET_SDKLIBS_ADDR	"sdklibs.addr"

void config_build(config_t *config);
void config_print(const config_t *config);

/*
 * libconfig wrapper functions for lazy people.
 */

static inline int config_get_int(const config_t *config, const char *path)
{
	return config_setting_get_int(config_lookup(config, path));
}

static inline long long config_get_int64(const config_t *config, const char *path)
{
	return config_setting_get_int64(config_lookup(config, path));
}

static inline double config_get_float(const config_t *config, const char *path)
{
	return config_setting_get_float(config_lookup(config, path));
}

static inline int config_get_bool(const config_t *config, const char *path)
{
	return config_setting_get_bool(config_lookup(config, path));
}

static inline const char *config_get_string(const config_t *config, const char *path)
{
	return config_setting_get_string(config_lookup(config, path));
}

static inline const char *config_get_string_elem(const config_t *config, const char *path, int index)
{
	return config_setting_get_string_elem(config_lookup(config, path), index);
}

#endif /* _CONFIGMAN_H_ */
