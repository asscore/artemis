/*
 * Manage cheat codes
 */

#include "myutil.h"
#include "cheatman.h"

/*
 * Load cheats from text file.
 */
int load_cheats(const char *cheatfile, cheats_t *cheats)
{
	char *buf = NULL;
	int ret;

	cheats_destroy(cheats);

	buf = read_text_file(cheatfile);
	if (buf == NULL) {
		fprintf(stderr, "%s: could not read cheats file '%s'\n",
			__FUNCTION__, cheatfile);
		return -1;
	}

	cheats_init(cheats);
	ret = cheats_read_buf(cheats, buf);
	free(buf);
	if (ret != CHEATS_TRUE) {
		fprintf(stderr, "%s: line %i: %s\n", cheatfile,
			cheats->error_line, cheats->error_text);
		cheats_destroy(cheats);
		return -1;
	}

	return 0;
}

/*
 * Activate selected cheats.
 */
void activate_cheats(const game_t *game, engine_t *engine)
{
	cheat_t *cheat = NULL;
	code_t *code = NULL;
	int nextcodecanbehook = 1;

	CHEATS_FOREACH(cheat, &game->cheats) {
		CODES_FOREACH(code, &cheat->codes) {
			D_PRINTF("%08X %08X\n", code->addr, code->val);
			if (((code->addr & 0xfe000000) == 0x90000000) && nextcodecanbehook == 1)
				engine->add_hook(code->addr, code->val);
			else
				engine->add_code(code->addr, code->val);

			/* discard any false positives from being possible hooks */
			if ((code->addr & 0xf0000000) == 0x40000000 || 0x30000000)
				nextcodecanbehook = 0;
			else
				nextcodecanbehook = 1;
		}
	}
}

/*
 * Reset activated cheats.
 */
void reset_cheats(engine_t *engine)
{
	engine->clear_hooks();
	engine->clear_codes();
}
