/*
 * ERL file manager
 */

#ifndef _ERLMAN_H_
#define _ERLMAN_H_

#include <libconfig.h>
#include "engine.h"

int install_erls(const config_t *config, engine_t *engine);
void uninstall_erls(void);

#endif /* _ERLMAN_H_ */
