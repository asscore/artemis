/*
 * Manage cheat codes
 */

#ifndef _CHEATMAN_H_
#define _CHEATMAN_H_

#include <libcheats.h>
#include "engine.h"

int load_cheats(const char *cheatfile, cheats_t *cheats);
void activate_cheats(const game_t *game, engine_t *engine);
void reset_cheats(engine_t *engine);

#endif /* _CHEATMAN_H_ */
