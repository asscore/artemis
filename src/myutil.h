/*
 * Useful utility functions
 */

#ifndef _MYUTIL_H_
#define _MYUTIL_H_

#include <kernel.h>
#include <sifrpc.h>
#include <iopcontrol.h>
#include <iopheap.h>
#include <loadfile.h>
#include <string.h>
#include "dbgprintf.h"

/* Device returned by get_dev() */
enum dev_id {
	DEV_CD,
	DEV_HOST,
	DEV_MASS,
	DEV_MC0,
	DEV_MC1,
	DEV_UNKN
};

static const char *dev_prefix[] = {
	[DEV_CD]   = "cdrom0:",
	[DEV_HOST] = "host:",
	[DEV_MASS] = "mass:",
	[DEV_MC0]  = "mc0:",
	[DEV_MC1]  = "mc1:",
	[DEV_UNKN] = NULL
};

/*
 * Get device from path.
 */
static inline enum dev_id get_dev(const char *path)
{
	int i = 0;

	while (dev_prefix[i] != NULL) {
		if (!strncmp(path, dev_prefix[i], strlen(dev_prefix[i])))
			return i;
		i++;
	}

	return DEV_UNKN;
}

/*
 * Resets the IOP.
 */
static inline void reset_iop(const char *img)
{
	D_PRINTF("%s: IOP says goodbye...\n", __FUNCTION__);

	SifInitRpc(0);

	while (!SifIopReset(img, 0))
		;
	while (!SifIopSync())
		;

	/* exit services */
	fioExit();
	SifExitIopHeap();
	SifLoadFileExit();
	SifExitRpc();
	SifExitCmd();

	FlushCache(0);
	FlushCache(2);

	/* init services */
	SifInitRpc(0); /* initialize SIFRPC and SIFCMD */
	SifLoadFileInit(); /* initialize LOADFILE RPC */
	SifInitIopHeap();
	fioInit(); /* initialize FILEIO RPC */

	FlushCache(0);
	FlushCache(2);
}

/*
 * Reads text from a file into a buffer.
 */
static inline char *read_text_file(const char *filename)
{
	char *buf = NULL;
	int fd, filesize;

	fd = open(filename, O_RDONLY);
	if (fd < 0) {
		fprintf(stderr, "%s: can't open text file %s\n",
				__FUNCTION__, filename);
		return NULL;
	}

	filesize = lseek(fd, 0, SEEK_END);
	buf = malloc(filesize + 1);
	if (buf == NULL) {
		fprintf(stderr, "%s: unable to allocate %i bytes\n",
			__FUNCTION__, filesize + 1);
		goto end;
	}

	if (filesize > 0) {
		lseek(fd, 0, SEEK_SET);
		if (read(fd, buf, filesize) != filesize) {
			fprintf(stderr, "%s: can't read from text file %s\n",
				__FUNCTION__, filename);
			free(buf);
			buf = NULL;
			goto end;
		}
	}

	buf[filesize] = '\0';
end:
	close(fd);
	return buf;
}

/*
 * Reads data from a file into a buffer.
 */
static inline void *read_file(const char *path, int *size)
{
	void *buffer = NULL;

	int fd = fioOpen(path, O_RDONLY);
	if (fd < 0) {
		fprintf(stderr, "%s: can't open file %s\n",
				__FUNCTION__, path);
		return NULL;
	}

	*size = fioLseek(fd, 0, SEEK_END);
	buffer = malloc(*size);
	if (buffer == NULL) {
		fprintf(stderr, "%s: unable to allocate %i bytes\n",
			__FUNCTION__, *size);
		goto end;
	}

	if (*size > 0) {
		fioLseek(fd, 0, SEEK_SET);
		if (fioRead(fd, buffer, *size) != *size) {
			fprintf(stderr, "%s: can't read from file %s\n",
				__FUNCTION__, path);
			free(buffer);
			buffer = NULL;
			goto end;
		}
	}

end:
	fioClose(fd);
	return buffer;
}

#ifdef _DVD

/*
 * Parse "cdrom0:\\SYSTEM.CNF;1" for BOOT2.
 */
static inline int parse_systemcnf(char *boot2)
{
	const char *sep = "\n";
	const char *entry = "BOOT2";
	char buf[256];
	char *ptr, *tok;
	int fd, n, found = 0;

	if (boot2 == NULL)
		return -1;

	fd = open("cdrom0:\\SYSTEM.CNF;1", O_TEXT | O_RDONLY);
	if (fd < 0) {
		fprintf(stderr, "%s: Can't open 'SYSTEM.CNF'\n", __FUNCTION__);
		return -2;
	}

	n = read(fd, buf, sizeof(buf) - 1);
	close(fd);
	if (!n) {
		fprintf(stderr, "%s: File size = 0\n", __FUNCTION__);
		return -3;
	}

	buf[n] = '\0';
	tok = strtok(buf, sep);

	while (tok != NULL) {
		D_PRINTF("%s: %s\n", __FUNCTION__, tok);

		if (!found) {
			ptr = strstr(tok, entry);
			if (ptr != NULL) {
				int x = 0;
				ptr += strlen(entry);
				while (isspace(*ptr) || (*ptr == '='))
					ptr++;
				strcpy(boot2, ptr);
				while (boot2[0] != '\\') { boot2++; } /* finds '\' in cdrom0:\\.... */
				boot2++; /* skips '\' */
				while (boot2[x] != ';') { x++; } /* finds the ';' in ????_???.??;1 */
				boot2[x+2] = '\0'; /* ends string at S???_???.??;1 */
				found = 1;
			}
		}

		tok = strtok(NULL, sep);
	}

	if (!found) {
		fprintf(stderr, "%s: Can't find %s entry\n", __FUNCTION__, entry);
		return -4;
	}

	return 0;
}

#endif

#endif /*_MYUTIL_H_*/
