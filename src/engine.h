/*
 * Interface to cheat engine
 */

#ifndef _ENGINE_H_
#define _ENGINE_H_

#include <tamtypes.h>

/* Cheat engine context */
typedef struct {
	int (*get_max_hooks)(void);
	int (*get_num_hooks)(void);
	int (*add_hook)(u32 addr, u32 val);
	void (*clear_hooks)(void);

	int (*get_max_codes)(void);
	void (*set_max_codes)(int num);
	int (*get_num_codes)(void);
	int (*add_code)(u32 addr, u32 val);
	void (*clear_codes)(void);

	int (*register_callback)(void *func);
} engine_t;

#endif /* _ENGINE_H_ */
