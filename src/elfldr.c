/*
 * ELF loader
 */

#include <tamtypes.h>
#include <kernel.h>
#include <sifrpc.h>
#include <fileio.h>
#include <libpad.h>
#ifdef _KEYBOARD
#include <libkbd.h>
#endif
#ifdef _DVD
#include <libcdvd.h>
#endif
#include <string.h>
#include "configman.h"
#include "elfldr.h"

#define PAD_PORT 0
#define PAD_SLOT 0

#define ELF_PT_LOAD 1

/* ELF file header */
typedef struct {
	u8 ident[16];	/* Magic number and other info */
	u16 type;		/* Object file type */
	u16 machine;	/* Architecture */
	u32 version;	/* Object file version */
	u32 entry;		/* Entry point virtual address */
	u32 phoff;		/* Program header table file offset */
	u32 shoff;		/* Section header table file offset */
	u32 flags;		/* Processor-specific flags */
	u16 ehsize;		/* ELF header size in bytes */
	u16 phentsize;	/* Program header table entry size */
	u16 phnum;		/* Program header table entry count */
	u16 shentsize;	/* Section header table entry size */
	u16 shnum;		/* Section header table entry count */
	u16 shstrndx;	/* Section header string table index */
} elf_header_t;

/* Program segment header */
typedef struct {
	u32 type;		/* Segment type */
	u32 offset;		/* Segment file offset */
	void *vaddr;	/* Segment virtual address */
	u32 paddr;		/* Segment physical address */
	u32 filesz;		/* Segment size in file */
	u32 memsz;		/* Segment size in memory */
	u32 flags;		/* Segment flags */
	u32 align;		/* Segment alignment */
} elf_pheader_t;

extern u8  _bootstrap_elf_start[];
extern int _bootstrap_elf_size;

/*
 * Load boostrap into memory and return entrypoint.
 */
static void* load_bootstrap(void)
{
	elf_header_t *eh = (elf_header_t *)_bootstrap_elf_start;
	elf_pheader_t *eph = (elf_pheader_t *)(_bootstrap_elf_start + eh->phoff);

	int i;
	for (i = 0; i < eh->phnum; i++) {
		if (eph[i].type != ELF_PT_LOAD)
			continue;

		void *pdata = (void *)(_bootstrap_elf_start + eph[i].offset);
		memcpy(eph[i].vaddr, pdata, eph[i].filesz);

		if (eph[i].memsz > eph[i].filesz)
			memset(eph[i].vaddr + eph[i].filesz, 0, eph[i].memsz - eph[i].filesz);
	}

	return (void *)eh->entry;
}

/*
 * Start ELF specified by path.
 */
void execute(const config_t *config, const char *path)
{
	static char boot2[100];

#ifdef _DVD
	/* wait for disc to be ready */
	while(sceCdGetDiskType() == SCECdDETCT)
		;
	sceCdDiskReady(0);
#endif

	strncpy(boot2, path, 100);

	void *bootstrap_entrypoint = load_bootstrap();

	padPortClose(PAD_PORT, PAD_SLOT);
#ifdef _KEYBOARD
	PS2KbdClose();
#endif

	fioExit();
	SifInitRpc(0);
	SifExitRpc();

	FlushCache(0); /* data cache */
	FlushCache(2); /* instruction cache */

	char *argv[3] = {boot2, "0", "\0"};

#ifdef _DVD
	if (!config_get_bool(config, SET_ENGINE_INSTALL)) {
		argv[1] = "1"; /* enable patches */
	}
#endif

	ExecPS2(bootstrap_entrypoint, 0, 3, argv);
}
