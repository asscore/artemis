/*
 * Graphical user interface
 */

#include <tamtypes.h>
#include <gsKit.h>
#include <stdio.h>
#include <stdlib.h>
#include "gui.h"

#ifdef _FONT_BITSUMISHI
/* Bitsumishi font width */
int font_width[96] = {
	 16,  8,  9, 18, 20, 18, 23,  8, 10, 10,  8, 16,  8, 15,  7, 18, /* Row 1 */
	 20, 12, 20, 20, 20, 20, 19, 20, 20, 20,  7,  7, 17, 16, 17, 24, /* Row 2 */
	  0, 24, 23, 21, 21, 21, 21, 21, 23,  6, 20, 20, 21, 25, 22, 23, /* Row 3 */
	 24, 24, 24, 23, 22, 24, 24, 28, 23, 23, 23, 11, 20, 11, 15, 22, /* Row 4 */
	 15, 25, 23, 21, 21, 21, 21, 21, 23,  6, 20, 20, 21, 25, 22, 23, /* Row 5 */
	 24, 24, 24, 23, 22, 24, 24, 28, 23, 23, 23, 15,  8, 15, 15, 15  /* Row 6 */
};
#else
/* Neuropol font width */
int font_width[96] = {
	 16,  6, 10, 22, 21, 28, 24,  6,  8,  8, 16, 12,  6, 13,  6, 21, /* Row 1 */
	 24,  7, 24, 25, 26, 25, 25, 24, 24, 24,  7,  7, 16, 14, 16, 17, /* Row 2 */
	 19, 26, 23, 21, 24, 21, 21, 23, 23,  6, 12, 21, 21, 31, 23, 23, /* Row 3 */
	 23, 23, 22, 21, 20, 24, 25, 33, 22, 22, 22,  9, 20,  9, 11, 22, /* Row 4 */
	  9, 21, 20, 20, 21, 20, 11, 21, 21,  6, 10, 18,  6, 27, 20, 20, /* Row 5 */
	 20, 20, 10, 18, 12, 21, 23, 29, 21, 21, 20,  9,  6,  9,  0,  0  /* Row 6 */
};
#endif

GSGLOBAL *global;

/* Textures */
GSTEXTURE tex_background;
GSTEXTURE tex_header_line;
GSTEXTURE tex_font;

/* Raw textures */
extern u8  _background_png_start[];
extern int _background_png_size;
extern u8  _header_line_png_start[];
extern int _header_line_png_size;
extern u8  _font_png_start[];
extern int _font_png_size;

/* Screen defaults for NTSC, just in case */
int SCREEN_WIDTH	= 640;
int SCREEN_HEIGHT	= 448;
int SCREEN_X		= 632;
int SCREEN_Y		= 50;
float Y_RATIO		= 0.875f;

/*
 * PNG handling code.
 */
#ifdef _LIBPNG

#include <png.h>

typedef struct {
	int width;
	int height;
	int bit_depth;
	void *priv;
} pngData;

typedef struct {
	png_structp	png_ptr;
	png_infop info_ptr, end_info;
	u8 *buf;
	int pos;
	u8 *data;
} pngPrivate;

static void read_data_fn(png_structp png_ptr, png_bytep buf, png_size_t size)
{
	pngPrivate *priv = (pngPrivate*)png_get_io_ptr(png_ptr);

	memcpy(buf, priv->buf + priv->pos, size);
	priv->pos += size;
}

static pngData *pngOpenRAW(u8 *data, int size)
{
	pngData	*png;
	pngPrivate *priv;

	if (png_sig_cmp(data, 0, 8) != 0)
		return NULL;

	if ((png = malloc(sizeof(pngData))) == NULL)
		return NULL;

	memset (png, 0, sizeof(pngData));

	if ((priv = malloc(sizeof(pngPrivate))) == NULL)
		return NULL;

	png->priv = priv;

	priv->png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!priv->png_ptr) {
		free(png);
		return NULL;
	}

	priv->info_ptr = png_create_info_struct(priv->png_ptr);
	if (!priv->info_ptr) {
		free(png);
		png_destroy_read_struct(&priv->png_ptr, NULL, NULL);
		return NULL;
	}

	priv->end_info = png_create_info_struct(priv->png_ptr);
	if (!priv->end_info) {
		free(png);
		png_destroy_read_struct(&priv->png_ptr, &priv->info_ptr, NULL);
		return NULL;
	}

	priv->buf = data;
	priv->pos = 0;

	png_set_read_fn(priv->png_ptr, (png_voidp)priv, read_data_fn);
	png_read_png(priv->png_ptr, priv->info_ptr, PNG_TRANSFORM_IDENTITY, NULL);

	png->width = png_get_image_width(priv->png_ptr, priv->info_ptr);
	png->height = png_get_image_height(priv->png_ptr, priv->info_ptr);
	png->bit_depth = png_get_channels(priv->png_ptr, priv->info_ptr) * 8;

	return png;
}

static int pngReadImage(pngData *png, u8 *dest)
{
	pngPrivate *priv = png->priv;
	u8 **row_pointers;
	int i, row_ptr;

	int y;

	row_pointers = png_get_rows(priv->png_ptr, priv->info_ptr);
	row_ptr = 0;

	for (i = 0; i < png_get_image_height(priv->png_ptr, priv->info_ptr); i++) {
		memcpy(dest + row_ptr, row_pointers[i], png_get_rowbytes(priv->png_ptr, priv->info_ptr));

		/* need to normalize alpha channel to PS2 range */
		if (png_get_channels(priv->png_ptr, priv->info_ptr) == 4) {
			for (y = 3; y < png_get_rowbytes(priv->png_ptr, priv->info_ptr); y += 4)
				*(dest + row_ptr + y ) /= 2;
		}

		row_ptr += png_get_rowbytes(priv->png_ptr, priv->info_ptr);
	}

	return 1;
}

static void pngClose(pngData *png)
{
	pngPrivate *priv = png->priv;

	png_destroy_read_struct(&priv->png_ptr, &priv->info_ptr, &priv->end_info);

	if (priv->data)
		free(priv->data);

	free(priv);
	free(png);
}

static void load_png(GSTEXTURE *tex, u8 *data, int len, int linear_filtering)
{
	pngData *pPng;
	u8 *pImgData;

	if ((pPng = pngOpenRAW(data, len)) > 0) {
		if ((pImgData = malloc(pPng->width * pPng->height * (pPng->bit_depth / 8))) > 0) {
			if (pngReadImage( pPng, pImgData) != -1) {
				tex->PSM = GS_PSM_CT32;
				tex->Mem = (u32 *)pImgData;
				tex->VramClut = 0;
				tex->Clut = NULL;
				tex->Width = pPng->width;
				tex->Height = pPng->height;
				tex->Filter = (linear_filtering) ? GS_FILTER_LINEAR : GS_FILTER_NEAREST;
				tex->Vram = gsKit_vram_alloc(global,
					gsKit_texture_size(tex->Width, tex->Height, tex->PSM), GSKIT_ALLOC_USERBUFFER);
				gsKit_texture_upload(global, tex);
			}
			pngClose(pPng);
			free(pImgData);
		}
	}
}
#else

#include "upng.h"

static void load_png(GSTEXTURE *tex, u8 *data, int len, int linear_filtering)
{
	upng_t* png_texture = upng_new_from_bytes(data, len);
	upng_header(png_texture);
	upng_decode(png_texture);

	tex->VramClut = 0;
	tex->Clut = NULL;
	tex->Width = upng_get_width(png_texture);
	tex->Height = upng_get_height(png_texture);
	u8 *image_buffer = (u8 *)upng_get_buffer(png_texture);

	if (upng_get_format(png_texture) == UPNG_RGB8) {
		tex->PSM = GS_PSM_CT24;
	} else if (upng_get_format(png_texture) == UPNG_RGBA8) {
		tex->PSM = GS_PSM_CT32;

		/* convert alpha value range to [0, 80] */
		int i;
		for (i = 0; i < tex->Width * tex->Height; i++) {
			u8 alpha = image_buffer[i * 4 + 3];

			if (alpha == 0xFF) {
				alpha = 0x80;
			} else {
				alpha = alpha >> 1;
			}

			image_buffer[i * 4 + 3] = alpha;
		}
	}

	tex->Mem = memalign(128, gsKit_texture_size_ee(tex->Width, tex->Height, tex->PSM));
	memcpy(tex->Mem, image_buffer, gsKit_texture_size_ee(tex->Width, tex->Height, tex->PSM));
	tex->Vram = gsKit_vram_alloc(global, gsKit_texture_size(tex->Width, tex->Height, tex->PSM), GSKIT_ALLOC_USERBUFFER);
	tex->Filter = (linear_filtering) ? GS_FILTER_LINEAR : GS_FILTER_NEAREST;
	gsKit_texture_upload(global, tex);

	upng_free(png_texture);
}
#endif

/*
 * Setup GS.
 */
void setup_gs(void)
{
	int gs_vmode, fdn;
	u8  romver[16];

	/* defaults to NTSC mode */
	gs_vmode = GS_MODE_NTSC;

	/* reading ROMVER */
	if ((fdn = open("rom0:ROMVER", O_RDONLY)) > 0) {
		read(fdn, romver, sizeof romver);
		close(fdn);
	}

	/* set video PAL mode if needed */
	if (romver[4] == 'E')
		gs_vmode = GS_MODE_PAL;

	/* set default screen values to use with gsKit depending on TV mode */
	if (gs_vmode == GS_MODE_PAL) { /* PAL values fit perfectly on my TV */
		SCREEN_WIDTH  = 640;
		SCREEN_HEIGHT = 512;
		SCREEN_X	  = 692;
		SCREEN_Y	  = 72;
		Y_RATIO		  = 1.0f;
	} else { /* NTSC values can be adjusted, altought it fit fine too on my TV */
		SCREEN_WIDTH  = 640;
		SCREEN_HEIGHT = 448;
		SCREEN_X 	  = 672;
		SCREEN_Y	  = 50;
		Y_RATIO		  = 0.875f;
	}

	/* initialize the GS */
	global = gsKit_init_global_custom(
		GS_RENDER_QUEUE_OS_POOLSIZE,
		GS_RENDER_QUEUE_PER_POOLSIZE);

	/* clear screen */
	gsKit_clear(global, GS_SETREG_RGBAQ(0x00,0x00,0x00,0x80,0x00));

	/* mode & screen width/height init */
	global->Mode = gs_vmode;
	global->Width = SCREEN_WIDTH;
	global->Height = SCREEN_HEIGHT;

	/* screen position init */
	global->StartX = SCREEN_X;
	global->StartY = SCREEN_Y;

	/* buffer init */
	global->PrimAlphaEnable = GS_SETTING_ON;
	global->PrimAAEnable = GS_SETTING_ON;
	global->DoubleBuffering = GS_SETTING_OFF;
	global->ZBuffering = GS_SETTING_OFF;
	global->PSM = GS_PSM_CT32;

	/* force interlace and field mode */
	global->Interlace = GS_INTERLACED;
	global->Field = GS_FIELD;

	/* initialize the DMAC */
	dmaKit_init(D_CTRL_RELE_OFF, D_CTRL_MFD_OFF, D_CTRL_STS_UNSPEC,
			D_CTRL_STD_OFF, D_CTRL_RCYC_8, 1 << DMA_CHANNEL_GIF);

	dmaKit_chan_init(DMA_CHANNEL_GIF);
	dmaKit_chan_init(DMA_CHANNEL_FROMSPR);
	dmaKit_chan_init(DMA_CHANNEL_TOSPR);

	/* screen init */
	gsKit_init_screen(global);
	gsKit_clear(global, GS_SETREG_RGBAQ(0x00,0x00,0x00,0x80,0x00));

	gsKit_mode_switch(global, GS_PERSISTENT);

	/* clears VRAM  */
	gsKit_vram_clear(global);

	load_png(&tex_background, _background_png_start, _background_png_size, 1);
	load_png(&tex_header_line, _header_line_png_start, _header_line_png_size, 1);
	load_png(&tex_font, _font_png_start, _font_png_size, 1);
}

/*
 * Start a new render frame.
 */
void begin_frame(void)
{
	gsKit_clear(global, GS_SETREG_RGBAQ(0x00,0x00,0x00,0x80,0x00));

	/* set alpha settings */
	gsKit_set_primalpha(global, GS_SETREG_ALPHA(0,1,0,1,0), 0);
	gsKit_set_test(global, GS_ATEST_OFF);
}

/*
 * Finish rendering.
 */
void end_frame(void)
{
	gsKit_set_test(global, GS_ATEST_ON);
	/* blend alpha primitives "Back To Front" */
	gsKit_set_primalpha(global, GS_BLEND_BACK2FRONT, 0);

	/* flips framebuffers on VSync */
	gsKit_sync_flip(global);
	/* normal user draw queue "Execution" (kicks Oneshot and Persistent queues) */
	gsKit_queue_exec(global);
	gsKit_queue_reset(global->Per_Queue);
}

/*
 * Draw background texture.
 */
void draw_background(int alpha)
{
	gsKit_prim_sprite_texture(global, &tex_background,
							0,					   /* x1 */
							0,					   /* y1 */
							0,					   /* u1 */
							0,					   /* v1 */
							SCREEN_WIDTH,		   /* x2 */
							SCREEN_HEIGHT,		   /* y2 */
							tex_background.Width,  /* u2 */
							tex_background.Height, /* v2 */
							0,
							GS_SETREG_RGBAQ(0x80, 0x80, 0x80, alpha, 0x00));
}

/*
 * Draw header delimeter.
 */
void draw_header_line(int x, int y, int alpha)
{
	y *= Y_RATIO;

	gsKit_prim_sprite_texture(global, &tex_header_line,
							x,										/* x1 */
							y,										/* y1 */
							0,										/* u1 */
							0,										/* v1 */
							x + SCREEN_WIDTH,		 				/* x2 */
							y + (tex_header_line.Height * Y_RATIO),	/* y2 */
							tex_header_line.Width, 					/* u2 */
							tex_header_line.Height,					/* v2 */
							0,
							GS_SETREG_RGBAQ(0x80, 0x80, 0x80, alpha, 0x00));
}

/*
 * Draw a character with neuropol/bitsumishi font.
 */
static void draw_char(float x, float y, int alpha, u32 width, u32 height, u32 c)
{
	float x1, x2, y1, y2;
	int u1, u2, v1, v2;

	x1 = x;
	x2 = x1 + width;
	y1 = y;
	y2 = y1 + height;

	c -= 32;
	u1 = (c % (tex_font.Width/32)) * (tex_font.Width/16);
	u2 = u1 + 32;
	v1 = (c - (c % 16)) * 2; /* careful: 6 rows only !!! */
	v2 = v1 + 32;

	/* draw a char using neuropol/bitsumishi texture */
	gsKit_prim_sprite_texture(global, &tex_font,
							x1,	/* x1 */
							y1,	/* y1 */
							u1,	/* u1 */
							v1,	/* v1 */
							x2,	/* x2 */
							y2,	/* y2 */
							u2,	/* u2 */
							v2,	/* v2 */
							0,
							GS_SETREG_RGBAQ(0x00, 0x00, 0x00, alpha, 0x00));
}

/*
 * Draw a string with neuropol/bitsumishi font.
 */
void draw_string(u32 x, u32 y, int alpha, int fontsize, int fontspacing, const char *string)
{
	int l, i;
	float cx;
	int c;

	y *= Y_RATIO;

	cx = x;

	l = strlen(string);

	for (i = 0; i < l; i++)	{
		c = (u8)string[i];

		/* catch "\n" */
		if (c == 10) {
			y += fontsize * Y_RATIO;
			cx = x;
		}

		/* draw the string character by character */
		draw_char(cx, y, alpha, fontsize, fontsize * Y_RATIO, c);

		/* uses width informations for neuropol/bitsumishi font header file */
		if (c != 10) {
			float f = font_width[c-32] * (float)(fontsize / 32.0f);
			cx += (float)(f + fontspacing);
		}
	}
}

/*
 * Calculate and return width in pixels of a string using neuropol/bitsumishi font.
 */
static int string_width(const char *string, int fontsize, int fontspacing)
{
	int i, l, c;
	float size;

	l = strlen(string);

	size = 0;

	for (i = 0; i < l; i++) {
		c = (u8)string[i];
		float f = font_width[c-32] * (float)(fontsize / 32.0f);
		size += (float)(f + fontspacing);
	}

	return (int)size;
}

/*
 * Draw a right aligned string with neuropol/bitsumishi font.
 */
void draw_string_right(u32 x, u32 y, int alpha, int fontsize, int fontspacing, const char *string)
{
	int l, i;
	float cx;
	int c;

	y *= Y_RATIO;

	cx = x - string_width(string, fontsize, fontspacing);

	l = strlen(string);

	for (i = 0; i < l; i++)	{
		c = (u8)string[i];

		/* catch "\n" */
		if (c == 10) {
			y += fontsize * Y_RATIO;
			cx = x;
		}

		/* draw the string character by character */
		draw_char(cx, y, alpha, fontsize, fontsize * Y_RATIO, c);

		/* uses width informations for neuropol/bitsumishi font header file */
		if (c != 10) {
			float f = font_width[c-32] * (float)(fontsize / 32.0f);
			cx += (float)(f + fontspacing);
		}
	}
}
