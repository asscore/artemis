/*
 * IRX file manager
 */

#ifndef _IRXMAN_H_
#define _IRXMAN_H_

#include <libconfig.h>

int load_modules(const config_t *config);

#endif /* _IRXMAN_H_ */
