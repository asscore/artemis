/*
 * Configuration manager
 */

#include <tamtypes.h>
#include <stdio.h>
#include <libconfig.h>
#include "dbgprintf.h"
#include "configman.h"

/*
 * Build configuration with all required settings.
 */
void config_build(config_t *config)
{
	config_setting_t *root, *group, *set;

	config_init(config);
	root = config_root_setting(config);

	/*
	 * loader section
	 */
	group = config_setting_add(root, "loader", CONFIG_TYPE_GROUP);

	set = config_setting_add(group, "iop_reset", CONFIG_TYPE_BOOL);
#ifdef IOP_RESET
	config_setting_set_bool(set, IOP_RESET);
#endif
	set = config_setting_add(group, "sbv_patches", CONFIG_TYPE_BOOL);
#ifdef SBV_PATCHES
	config_setting_set_bool(set, SBV_PATCHES);
#endif
	set = config_setting_add(group, "usb_support", CONFIG_TYPE_BOOL);
#ifdef USB_SUPPORT
	config_setting_set_bool(set, USB_SUPPORT);
#endif
	set = config_setting_add(group, "boot2", CONFIG_TYPE_STRING);
#ifdef BOOT2_FILE
	config_setting_set_string(set, BOOT2_FILE);
#endif
	set = config_setting_add(group, "cheats", CONFIG_TYPE_STRING);
#ifdef CHEATS_FILE
	config_setting_set_string(set, CHEATS_FILE);
#endif

	/*
	 * engine section
	 */
	group = config_setting_add(root, "engine", CONFIG_TYPE_GROUP);

	set = config_setting_add(group, "install", CONFIG_TYPE_BOOL);
#ifdef ENGINE_INSTALL
	config_setting_set_bool(set, ENGINE_INSTALL);
#endif
	set = config_setting_add(group, "addr", CONFIG_TYPE_INT);
#ifdef ENGINE_ADDR
	config_setting_set_int(set, ENGINE_ADDR);
#endif

	/*
	 * sdklibs section
	 */
	group = config_setting_add(root, "sdklibs", CONFIG_TYPE_GROUP);

	set = config_setting_add(group, "install", CONFIG_TYPE_BOOL);
#ifdef SDKLIBS_INSTALL
	config_setting_set_bool(set, SDKLIBS_INSTALL);
#endif
	set = config_setting_add(group, "addr", CONFIG_TYPE_INT);
#ifdef SDKLIBS_ADDR
	config_setting_set_int(set, SDKLIBS_ADDR);
#endif
}

/*
 * Print out all config settings.
 */
void config_print(const config_t *config)
{
	D_PRINTF("Config settings:\n");

#define PRINT_BOOL(key) \
	D_PRINTF("%s = %i\n", key, config_get_bool(config, key))
#define PRINT_INT(key) \
	D_PRINTF("%s = %i\n", key, config_get_int(config, key))
#define PRINT_HEX(key) \
	D_PRINTF("%s = %08x\n", key, config_get_int(config, key))
#define PRINT_STRING(key) \
	D_PRINTF("%s = %s\n", key, config_get_string(config, key))

	/* loader */
	PRINT_BOOL(SET_IOP_RESET);
	PRINT_BOOL(SET_SBV_PATCHES);
	PRINT_BOOL(SET_USB_SUPPORT);
	PRINT_STRING(SET_BOOT2);
	PRINT_STRING(SET_CHEATS_FILE);

	/* engine */
	PRINT_BOOL(SET_ENGINE_INSTALL);
	PRINT_HEX(SET_ENGINE_ADDR);

	/* sdklibs */
	PRINT_BOOL(SET_SDKLIBS_INSTALL);
	PRINT_HEX(SET_SDKLIBS_ADDR);
}
