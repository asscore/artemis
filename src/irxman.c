/*
 * IRX file manager
 */

#include <tamtypes.h>
#include <loadfile.h>
#include <stdio.h>
#include <unistd.h>
#include "configman.h"
#include "myutil.h"
#include "irxman.h"

#ifndef _DTL_T10000
/* Default IOP modules to load */
static const char *_modules[] = {
	"rom0:SIO2MAN",
	"rom0:MCMAN",
	"rom0:MCSERV",
	"rom0:PADMAN",
	NULL
};
#endif

#ifdef _DTL_T10000
extern u8  _sio2man_irx_start[];
extern int _sio2man_irx_size;
extern u8  _mcman_irx_start[];
extern int _mcman_irx_size;
extern u8  _mcserv_irx_start[];
extern int _mcserv_irx_size;
extern u8  _padman_irx_start[];
extern int _padman_irx_size;
#endif

extern u8  _iomanX_irx_start[];
extern int _iomanX_irx_size;
extern u8  _usbd_irx_start[];
extern int _usbd_irx_size;
extern u8  _usbhdfsd_irx_start[];
extern int _usbhdfsd_irx_size;
#ifdef _KEYBOARD
extern u8  _ps2kbd_irx_start[];
extern int _ps2kbd_irx_size;
#endif

void *usbd_irx = NULL;
int   size_usbd_irx = 0;

#ifdef _LZMA2

#include <lzma2.h>

int SifExecDecompModuleBuffer(void *ptr, u32 size, u32 arg_len, const char *args, int *mod_res)
{
	char *irx_data;
	int irx_size, ret = -1;

	if ((irx_size = lzma2_get_uncompressed_size(ptr, size)) > 0) {
		irx_data = (char *)memalign(64, irx_size);
		ret = lzma2_uncompress(ptr, size, irx_data, irx_size);

		if (ret > 0)
			ret = SifExecModuleBuffer(irx_data, irx_size, arg_len, args, mod_res);

		free(irx_data);
	}

	return ret;
}

#endif

/*
 * Load IRX modules into IOP RAM.
 */
int load_modules(const config_t *config)
{
#ifdef _DTL_T10000
	int ret;
#else
	const char **modv = _modules;
	int i = 0, ret;
#endif

#ifdef _DTL_T10000
	SifExecModuleBuffer(_sio2man_irx_start, _sio2man_irx_size, 0, NULL, &ret);
	if (ret < 0) {
		fprintf(stderr, "%s: failed to load module freesio2.irx (%i)\n",
			__FUNCTION__, ret);
	}
	SifExecModuleBuffer(_mcman_irx_start, _mcman_irx_size, 0, NULL, &ret);
	if (ret < 0) {
		fprintf(stderr, "%s: failed to load module mcman.irx (%i)\n",
			__FUNCTION__, ret);
	}
	SifExecModuleBuffer(_mcserv_irx_start, _mcserv_irx_size, 0, NULL, &ret);
	if (ret < 0) {
		fprintf(stderr, "%s: failed to load module mcman.irx (%i)\n",
			__FUNCTION__, ret);
	}
	SifExecModuleBuffer(_padman_irx_start, _padman_irx_size, 0, NULL, &ret);
	if (ret < 0) {
		fprintf(stderr, "%s: failed to load module freepad.irx (%i)\n",
			__FUNCTION__, ret);
	}
#else
	while (modv[i] != NULL) {
		ret = SifLoadModule(modv[i], 0, NULL);
		if (ret < 0) {
			fprintf(stderr, "%s: failed to load module: %s (%i)\n",
				__FUNCTION__, modv[i], ret);
			return -1;
		}
		i++;
	}
#endif

#ifdef _LZMA2
	SifExecDecompModuleBuffer(_iomanX_irx_start, _iomanX_irx_size, 0, NULL, &ret);
#else
	SifExecModuleBuffer(_iomanX_irx_start, _iomanX_irx_size, 0, NULL, &ret);
#endif
	if (ret < 0) {
		fprintf(stderr, "%s: failed to load module iomanX.irx (%i)\n",
			__FUNCTION__, ret);
	}

	if (config_get_bool(config, SET_USB_SUPPORT)) {
		/* first search for a custom USBD module */
		usbd_irx = read_file("mc0:BEDATA-SYSTEM/USBD.IRX", &size_usbd_irx);
		if (!usbd_irx) {
			usbd_irx = read_file("mc0:BADATA-SYSTEM/USBD.IRX", &size_usbd_irx);
			if (!usbd_irx) {
				usbd_irx = read_file("mc0:BIDATA-SYSTEM/USBD.IRX", &size_usbd_irx);
				if (!usbd_irx) { /* if don't exist it uses embedded */
					usbd_irx = (void *)&_usbd_irx_start;
					size_usbd_irx = _usbd_irx_size;
				}
			}
		}

#ifdef _LZMA2
		SifExecDecompModuleBuffer(usbd_irx, size_usbd_irx, 0, NULL, &ret);
#else
		SifExecModuleBuffer(usbd_irx, size_usbd_irx, 0, NULL, &ret);
#endif
		if (ret < 0) {
			fprintf(stderr, "%s: failed to load module usbd.irx (%i)\n",
				__FUNCTION__, ret);
		}

#ifdef _LZMA2
		SifExecDecompModuleBuffer(_usbhdfsd_irx_start, _usbhdfsd_irx_size, 0, NULL, &ret);
#else
		SifExecModuleBuffer(_usbhdfsd_irx_start, _usbhdfsd_irx_size, 0, NULL, &ret);
#endif
		if (ret < 0) {
			fprintf(stderr, "%s: failed to load module usbhdfsd.irx (%i)\n",
				__FUNCTION__, ret);
		}

#ifdef _KEYBOARD
		SifExecModuleBuffer(_ps2kbd_irx_start, _ps2kbd_irx_size, 0, NULL, &ret);
		if (ret < 0) {
			fprintf(stderr, "%s: failed to load module ps2kbd.irx (%i)\n",
				__FUNCTION__, ret);
		}
#endif

		sleep(1); /* allow USB devices some time to be detected */
	}

	return 0;
}
