/*
 * ELF loader
 */

#ifndef _ELFLDR_H_
#define _ELFLDR_H_

#include <libconfig.h>

void execute(const config_t *config, const char *path);

#endif /* _ELFLDR_H_ */
