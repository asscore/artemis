/*
 * Graphical user interface
 */

#ifndef _GUI_H_
#define _GUI_H_

#include <tamtypes.h>

void setup_gs(void);
void begin_frame(void);
void end_frame(void);
void draw_background(int alpha);
void draw_header_line(int x, int y, int alpha);
void draw_string(u32 x, u32 y, int alpha, int fontsize, int fontspacing, const char *string);
void draw_string_right(u32 x, u32 y, int alpha, int fontsize, int fontspacing, const char *string);

#endif /* _GUI_H_ */
