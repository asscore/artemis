/*
 * ERL file manager
 */

#include <tamtypes.h>
#include <kernel.h>
#include <erl.h>
#include "dbgprintf.h"
#include "configman.h"
#include "erlman.h"

typedef struct {
	char name[20];
	u8 *start;
	struct erl_record_t *erl;
} erl_file_t;

enum {
	ERL_FILE_ENGINE = 0,
	ERL_FILE_LIBKERNEL,

	ERL_FILE_NUM /* number of files */
};

/* Statically linked ERL files */
extern u8 _engine_erl_start[];
extern u8 _libkernel_erl_start[];

static erl_file_t _erl_files[ERL_FILE_NUM] = {
	[ERL_FILE_ENGINE] = {
		.name = "engine.erl",
		.start = _engine_erl_start,
	},
	[ERL_FILE_LIBKERNEL] = {
		.name = "libkernel.erl",
		.start = _libkernel_erl_start,
	},
};

static int __install_erl(erl_file_t *file, u32 addr)
{
	D_PRINTF("%s: relocate %s at %08x\n", __FUNCTION__, file->name, addr);

	file->erl = load_erl_from_mem_to_addr(file->start, addr, 0, NULL);
	if (file->erl == NULL) {
		fprintf(stderr, "%s: %s load error\n", __FUNCTION__, file->name);
		return -1;
	}

	file->erl->flags |= ERL_FLAG_CLEAR;

	FlushCache(0);

	D_PRINTF("%s: size=%u end=%08x\n", __FUNCTION__, file->erl->fullsize,
		addr + file->erl->fullsize);

	return 0;
}

static int __uninstall_erl(erl_file_t *file)
{
	D_PRINTF("%s: uninstall %s from %08x\n", __FUNCTION__, file->name,
		(u32)file->erl->bytes);

	if (!unload_erl(file->erl)) {
		fprintf(stderr, "%s: %s unload error\n", __FUNCTION__, file->name);
		return -1;
	}

	file->erl = NULL;

	return 0;
}

static struct erl_record_t *__init_load_erl_dummy(char *erl_id)
{
	/* do nothing */
	return NULL;
}

/*
 * Install ERLs.
 */
int install_erls(const config_t *config, engine_t *engine)
{
	erl_file_t *file = NULL;
	struct symbol_t *sym = NULL;
	u32 addr;

	/* replace original load function - we resolve dependencies manually */
	_init_load_erl = __init_load_erl_dummy;

#define GET_SYMBOL(var, name) \
	sym = erl_find_local_symbol(name, file->erl); \
	if (sym == NULL) { \
		fprintf(stderr, "%s: could not find symbol '%s'\n", __FUNCTION__, name); \
		return -1; \
	} \
	D_PRINTF("%08x %s\n", (u32)sym->address, name); \
	var = (typeof(var))sym->address

	/*
	 * install SDK libraries
	 */
	if (config_get_bool(config, SET_SDKLIBS_INSTALL)) {
		addr = config_get_int(config, SET_SDKLIBS_ADDR);
		file = &_erl_files[ERL_FILE_LIBKERNEL];
		if (__install_erl(file, addr) < 0)
			return -1;
	} else {
		/* export syscall functions if libkernel isn't installed */
		erl_add_global_symbol("GetSyscallHandler", (u32)GetSyscallHandler);
		erl_add_global_symbol("SetSyscall", (u32)SetSyscall);
	}

	/*
	 * install cheat engine
	 */
	if (config_get_bool(config, SET_ENGINE_INSTALL)) {
		addr = config_get_int(config, SET_ENGINE_ADDR);
		file = &_erl_files[ERL_FILE_ENGINE];

		if (__install_erl(file, addr) < 0)
			return -1;

		/* populate engine context */
		GET_SYMBOL(engine->get_max_hooks, "get_max_hooks");
		GET_SYMBOL(engine->get_num_hooks, "get_num_hooks");
		GET_SYMBOL(engine->add_hook, "add_hook");
		GET_SYMBOL(engine->clear_hooks, "clear_hooks");
		GET_SYMBOL(engine->get_max_codes, "get_max_codes");
		GET_SYMBOL(engine->set_max_codes, "set_max_codes");
		GET_SYMBOL(engine->get_num_codes, "get_num_codes");
		GET_SYMBOL(engine->add_code, "add_code");
		GET_SYMBOL(engine->clear_codes, "clear_codes");
		GET_SYMBOL(engine->register_callback, "register_callback");
	}

	return 0;
}

/*
 * Uninstall ERLs.
 */
void uninstall_erls(void)
{
	erl_file_t *file;
	int i;

	for (i = 0; i < ERL_FILE_NUM; i++) {
		file = &_erl_files[i];
		if (file->erl != NULL) {
			__uninstall_erl(file);
		}
	}
}
