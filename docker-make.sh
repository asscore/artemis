#!/bin/bash
DOCKER_IMAGE=asscore/ps2dev-docker
docker run -v "$PWD:/build" --rm "$DOCKER_IMAGE" make "$@"
