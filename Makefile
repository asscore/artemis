#
# Makefile for boot loader
#

-include config.mak

EE_BIN = artemis.elf
EE_BIN_PACKED = artemis-packed.elf

# IRX modules
IRX_OBJS += resources/iomanX_irx.o
IRX_OBJS += resources/usbd_irx.o
IRX_OBJS += resources/usbhdfsd_irx.o
ifeq ($(KEYBOARD),1)
	IRX_OBJS += resources/ps2kbd_irx.o
endif
ifeq ($(DTL_T10000),1)
	IRX_OBJS += resources/sio2man_irx.o
	IRX_OBJS += resources/mcman_irx.o
	IRX_OBJS += resources/mcserv_irx.o
	IRX_OBJS += resources/padman_irx.o
endif

# Graphic resources
OBJS += resources/background_png.o
OBJS += resources/header_line_png.o
OBJS += resources/font_png.o

# Boot loader
OBJS += src/cheatman.o
OBJS += src/configman.o
OBJS += src/elfldr.o
OBJS += src/erlman.o
OBJS += src/gui.o
OBJS += src/irxman.o
OBJS += src/loader.o
ifneq ($(LIBPNG),1)
	OBJS += src/upng.o
endif

# Engine
OBJS += engine/engine_erl.o

# libkernel
OBJS += src/libkernel_erl.o

# Bootstrap
OBJS += bootstrap/bootstrap_elf.o

EE_LIBS += -lcheats
EE_LIBS += -lconfig
EE_LIBS += -lerl
EE_LIBS += -lpatches
EE_LIBS += -lgskit
EE_LIBS += -ldmakit
EE_LIBS += -lmc

ifeq ($(DTL_T10000),1)
	EE_CFLAGS += -D_DTL_T10000
	EE_LIBS += -lpadx
else
	EE_LIBS += -lpad
endif

ifeq ($(DVD),1)
	EE_CFLAGS += -D_DVD
	EE_LIBS += -lcdvd
endif

ifeq ($(KEYBOARD),1)
	EE_CFLAGS += -D_KEYBOARD
	EE_LIBS += -lkbd
endif

ifeq ($(DEBUG),1)
	EE_CFLAGS += -D_DEBUG
	EE_CFLAGS += -g
	EE_LDFLAGS += -g
else
	EE_LDFLAGS += -s
endif

EE_LDFLAGS += -L$(GSKIT)/lib
EE_LDFLAGS += -L./libcheats/lib
EE_LDFLAGS += -L./libconfig/lib

EE_INCS += -I$(GSKIT)/include
EE_INCS += -I./libcheats/include
EE_INCS += -I./libconfig/include

ifeq ($(LIBPNG),1)
	EE_CFLAGS += -D_LIBPNG
	EE_LDFLAGS += -L$(PS2SDK)/ports/lib
	EE_INCS += -I$(PS2SDK)/ports/include
	EE_LIBS += -Xlinker --start-group
	EE_LIBS += -lm -lz -lpng
	EE_LIBS += -Xlinker --end-group
endif

ifeq ($(FONT_BITSUMISHI),1)
	EE_CFLAGS += -D_FONT_BITSUMISHI
endif

ifeq ($(LZMA2),1)
	EE_CFLAGS += -D_LZMA2
	EE_INCS += -I$(PS2SDK)/ports/include
	EE_LDFLAGS += -L$(PS2SDK)/ports/lib
	EE_LIBS += -lxz
endif

EE_OBJS = $(IRX_OBJS) $(OBJS)

all: modules version main

modules:
	@# IRX modules
ifeq ($(LZMA2),1)
	@cp $(PS2SDK)/iop/irx/iomanX.irx iomanX.irx
	@xz --lzma2=nice=32 --check=crc32 iomanX.irx
	@bin2o iomanX.irx.xz resources/iomanX_irx.o _iomanX_irx
	@rm iomanX.irx.xz
else
	@bin2o $(PS2SDK)/iop/irx/iomanX.irx resources/iomanX_irx.o _iomanX_irx
endif
ifeq ($(LZMA2),1)
	@cp $(PS2SDK)/iop/irx/usbd.irx usbd.irx
	@xz --lzma2=nice=32 --check=crc32 usbd.irx
	@bin2o usbd.irx.xz resources/usbd_irx.o _usbd_irx
	@rm usbd.irx.xz
else
	@bin2o $(PS2SDK)/iop/irx/usbd.irx resources/usbd_irx.o _usbd_irx
endif
ifeq ($(LZMA2),1)
	@cp $(PS2SDK)/iop/irx/usbhdfsd.irx usbhdfsd.irx
	@xz --lzma2=nice=32 --check=crc32 usbhdfsd.irx
	@bin2o usbhdfsd.irx.xz resources/usbhdfsd_irx.o _usbhdfsd_irx
	@rm usbhdfsd.irx.xz
else
	@bin2o $(PS2SDK)/iop/irx/usbhdfsd.irx resources/usbhdfsd_irx.o _usbhdfsd_irx
endif
ifeq ($(KEYBOARD),1)
	@bin2o $(PS2SDK)/iop/irx/ps2kbd.irx resources/ps2kbd_irx.o _ps2kbd_irx
endif
ifeq ($(DTL_T10000),1)
	@bin2o $(PS2SDK)/iop/irx/freesio2.irx resources/sio2man_irx.o _sio2man_irx
	@bin2o $(PS2SDK)/iop/irx/mcman.irx resources/mcman_irx.o _mcman_irx
	@bin2o $(PS2SDK)/iop/irx/mcserv.irx resources/mcserv_irx.o _mcserv_irx
	@bin2o $(PS2SDK)/iop/irx/freepad.irx resources/padman_irx.o _padman_irx
endif

	@# Graphics
	@bin2o resources/background.png resources/background_png.o _background_png
	@bin2o resources/header_line.png resources/header_line_png.o _header_line_png
ifeq ($(FONT_BITSUMISHI),1)
	@bin2o resources/font_bitsumishi.png resources/font_png.o _font_png
else
	@bin2o resources/font_neuropol.png resources/font_png.o _font_png
endif

	@# libcheats
	@$(MAKE) -C libcheats

	@# libconfig
	@$(MAKE) -C libconfig

	@# Engine
	@$(MAKE) -C engine
	@bin2o engine/engine.erl engine/engine_erl.o _engine_erl

	@# libkernel
	@bin2o $(PS2SDK)/ee/lib/libkernel.erl src/libkernel_erl.o _libkernel_erl

	@# Bootstrap
	@$(MAKE) ENABLE_PATCHES=1 -C bootstrap
	@bin2o bootstrap/bootstrap.elf bootstrap/bootstrap_elf.o _bootstrap_elf

version:
	@./version.sh > src/version.h

main: $(EE_BIN)
	rm -rf src/*.o
	rm -f resources/*.o
	rm -f bootstrap/*.elf bootstrap/*.o
	rm -f engine/*.erl engine/*.o
ifneq ($(DEBUG),1)
ifneq ($(LZMA2),1)
	ps2-packer $(EE_BIN) $(EE_BIN_PACKED)
else
	ee-strip --remove-section=.comment $(EE_BIN)
endif
endif

clean:
	rm -rf src/*.o *.elf
	rm -f resources/*.o
	$(MAKE) -C engine clean
	$(MAKE) -C bootstrap clean
	$(MAKE) -C libcheats clean
	$(MAKE) -C libconfig clean

run:
	ps2client reset; sleep 3
	ps2client execee host:$(EE_BIN)


include $(PS2SDK)/samples/Makefile.pref
include $(PS2SDK)/samples/Makefile.eeglobal
