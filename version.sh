#!/bin/bash

echo -n '#define GIT_VERSION "'
git describe --abbrev=0 | tr -d '\n'
echo '"'
